(function (angular) {
    'use strict';
    angular.module('events').service('venueService', service);

    //userService.$inject = ['$http'];
    function service() {
        return function (parseObject) {
            var Object = parseObject.attributes;
            Object.parseObject = parseObject;
            Object.attributes = angular.copy(parseObject.attributes);
            Object.name = parseObject.attributes.name;
            Object.get = function(key){
                return this.attributes[key];
            };
            Object.getId = function(){
                return this.parseObject.id ;
            };
            return Object;
        }
    }
})(angular);

