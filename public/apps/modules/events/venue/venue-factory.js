(function (angular) {
    'use strict';
    angular.module('events').service('venueFactory', venueFactory);

    venueFactory.$inject = ['venueService'];

    function venueFactory(venueService) {
        var cache = [];
        return {
            getAll: getAll

        };



        function getAll(callback) {
            var object = Parse.Object.extend("Venue");
            var query = new Parse.Query(object);
            query.find({
                success: function (results) {
                    cache = results;
                    var events = [];
                    angular.forEach(results, function (row) {
                        events.push( (row) ) ;
                    });

                    callback(events);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

    }
})(angular);

