(function (angular) {
    'use strict';
    angular.module('events').service('userService', userService);

    //userService.$inject = ['$http'];
    function userService() {
        return function (parseObject) {
            var userObject = parseObject.attributes;
            userObject.parseObject = parseObject;
            userObject.attributes = angular.copy(parseObject.attributes);

            /*userObject.isActive = function(){
                if ( this.attributes['isActive']==true ){
                    return true;
                }
                return false;
            };*/

            userObject.get = function(key){
                return this.attributes[key];
            };



            userObject.getId = function(){
                return this.parseObject.id ;
            };


            return userObject;
        }
    }
})(angular);

