(function (angular) {
    'use strict';
    angular.module('events').service('userFactory', userFactory);

    userFactory.$inject = ['$http', 'userService'];

    function userFactory($http, userService) {
        return {
            getUserById: getUserById,
            store: store,
            login:login,
            auth:auth,
            logout:logout,
            remove:remove,
            update:update
        };

        function update (user_id, data, callback){
            data.hotelNumber = data.hotelNumber.id;
            $http.put('/users/'+user_id,data).success(function(answer){
                callback(answer);
            })
        }

        function remove(user_id,callback){
            //do request on our server. Because parse.com does not give remove users throw JS
            $http.delete('/users/'+user_id).success(function(answer){
                callback();
            })

        }

        function logout(callback){
            Parse.User.logOut();
                if ( callback!=undefined ){
                    callback();
                }
        }

        function auth(callback){
            var currentUser = Parse.User.current();
            if (currentUser) {
                getUserById(currentUser.id,function(user){

                        callback(user)

                })
            } else {
                callback(null)
            }
        }
        function login(username,password,callback){
            Parse.User.logIn(username, password, {
                success: function(user) {
                    var user = userService(user);

                        callback({success:true,user:userService(user)})

                },
                error: function(user, error) {
                    callback({success:false,error:error.message})
                }
            });
        }

        function store(data, callback) {
            var userObject = new Parse.User();
            var i;
            data['userType']='Admin';
            for (i in data) {
                userObject.set(i, data[i]);
            }

            userObject.save(null, {
                success: function (user) {
                    callback({success: true});
                },
                error: function (user, error) {
                    callback({success: false, error: error.message});
                }
            });
        }

        function getUserById(id, callback, wrapper) {

            var query = new Parse.Query(Parse.User);
            query.include("hotelNumber").get(id, {
                success: function (results) {
                    if (wrapper == undefined) {
                        callback(userService(results))
                    } else {
                        callback(results)
                    }
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }


        function getAllUsers(callback) {
            var hotel = Parse.Object.extend("_User");
            var query = new Parse.Query(hotel);
            query.include("hotelNumber").ascending("createdAt").find({
                success: function (results) {
                    var users = [];
                    angular.forEach(results, function (row) {
                        var userObject = userService(row);
                        users.push(userObject);
                    });
                    callback(users)
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

    }
})(angular);

