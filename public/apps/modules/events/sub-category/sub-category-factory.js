(function (angular) {
    'use strict';
    angular.module('events').service('subCategoryFactory', subCategoryFactory);

    subCategoryFactory.$inject = ['subCategoryService'];

    function subCategoryFactory(subCategoryService) {
        var cache = [];
        return {
            getAll: getAll
        };

        function getAll(callback) {
            var object = Parse.Object.extend("SubCategory");
            var query = new Parse.Query(object);
            query.include('Category');
            query.find({
                success: function (results) {
                    cache = results;
                    var events = [];
                    angular.forEach(results, function (row) {
                        events[ row.id ] = row;
                    });
                    callback(events);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

    }
})(angular);

