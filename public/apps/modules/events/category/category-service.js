(function (angular) {
    'use strict';
    angular.module('events').service('categoryService', categoryService);

    //userService.$inject = ['$http'];
    function categoryService() {
        return function (parseObject) {
            var Object = parseObject.attributes;
            Object.parseObject = parseObject;
            Object.attributes = angular.copy(parseObject.attributes);
            Object.get = function(key){
                return this.attributes[key];
            };
            Object.getId = function(){
                return this.parseObject.id ;
            };
            return Object;
        }
    }
})(angular);

