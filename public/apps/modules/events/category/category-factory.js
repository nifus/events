(function (angular) {
    'use strict';
    angular.module('events').service('categoryFactory', categoryFactory);

    categoryFactory.$inject = ['categoryService'];

    function categoryFactory(categoryService) {
        var cache = [];
        return {
            getAll: getAll

        };



        function getAll(callback) {
            var object = Parse.Object.extend("Category");
            var query = new Parse.Query(object);
            query.find({
                success: function (results) {
                    cache = results;
                    var events = {};
                    angular.forEach(results, function (row) {
                        events[ row.id ] = row;
                    });

                    callback(events);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

    }
})(angular);

