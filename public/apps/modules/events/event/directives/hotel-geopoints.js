(function (angular) {
    'use strict';

    function hotelGeopointsDirective() {
        return {
            ngModel: 'require',
            replace: true,
            restrict: 'E',
            controller: hotelGeopointsController,
            template: '' +
            '<div> <input type="text" class="form-control" name="geoLocation" id="geoLocation"' +
            'disabled="disable" ng-model="coords" required></div>',
            scope: {
                ngModel: '=',
                address: '@'
            }
        };

        hotelGeopointsController.$inject = ['$scope', '$http'];

        function hotelGeopointsController($scope, $http) {
            $scope.coords = '';
            $scope.$watchCollection('ngModel', function (value) {
                if (angular.isObject(value)) {
                    $scope.coords = 'Latitude: ' + value.latitude + ', Longitude:' + value.longitude;
                }
            });

            $scope.$watchCollection('address', function (value) {
                if (value == undefined) {
                    return false;
                }
                $http.post('/geo', {address: $scope.address}).success(function (result) {
                    if (result.error == undefined) {
                        $scope.ngModel = new Parse.GeoPoint({latitude: result.latitude, longitude: result.longitude});
                    }
                })
            })
        }


    }

    angular.module('hotelBooking').directive('hotelGeopoints', hotelGeopointsDirective);


})(window.angular);
