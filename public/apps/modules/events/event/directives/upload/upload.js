(function (angular) {
    'use strict';



    function uploadDirective() {
        return {
            ngModel:'require',
            replace:true,
            restrict: 'E',
            controller: uploadController,
            templateUrl:'/apps/modules/events/event/directives/upload/upload.html',
            scope:{
                ngModel:'='
            }
        };

        uploadController.$inject = ['$scope'];

        function uploadController($scope){
            $scope.preview = false;
            $scope.$watch('ngModel',function(value){
                if ( angular.isObject(value)===true ){
                    $scope.preview = true;
                }
            },true);

            $scope.$watch('file',function(value){
                if ( value==null ){
                    return false;
                }

                $scope.ngModel = new Parse.File( value.filename, { base64: value.base64 });
            },true)
        }


    }

    angular.module('events').directive('upload', uploadDirective);


})(window.angular);
