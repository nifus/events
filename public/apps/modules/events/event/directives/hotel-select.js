(function (angular) {
    'use strict';

    function hotelSelectDirective() {
        return {
            ngModel:'require',
            replace:true,
            restrict: 'E',
            controller:hotelSelectController,
            template: '' +
            '<div><select class="form-control" ng-model="selected" ng-options="hotel.id as hotel.get(\'name\')  for hotel in hotels">' +
            '<option value="">Select hotel</option>' +
            '</select></div>',
            scope:{
                ngModel:'='
            }
        };

        hotelSelectController.$inject = ['$scope','hotelFactory'];

        function hotelSelectController($scope,hotelFactory){

            $scope.hotels = [];
            //  load hotels
            hotelFactory.getParseAllHotels( function(hotels){
                $scope.hotels = hotels;
                $scope.$apply();
            });

            $scope.selected = "";

            $scope.$watchCollection('ngModel',function(value){
                if ( angular.isObject(value) ){
                    $scope.selected = value.id;
                }
            })

            $scope.$watchCollection('selected',function(value){
                $scope.hotels.filter(function(parseObject){
                    if ( parseObject.id==value){
                        $scope.ngModel = parseObject;
                    }
                })
            })
        }


    }

    angular.module('hotelBooking').directive('hotelSelect', hotelSelectDirective);


})(window.angular);
