(function (angular) {
    'use strict';



    function hotelDateDirective() {
        return {
            ngModel:'require',
            replace:true,
            restrict: 'E',
            controller:hotelDateController,
            template:
            '<div><input ng-disabled="ngDisabled" type="text" class="form-control" name="earliestCheckin" id="earliestCheckin"' +
            'placeholder="HH:MM"  ng-model="date" ></div>' ,

            scope:{
                ngModel:'=',
                ngDisabled:'='
            }
        };

        hotelDateDirective.$inject = ['$scope'];

        function hotelDateController($scope){
            var first_load = false;
            $scope.$watch('ngModel',function(value){
                if (value==undefined || first_load==true){
                    return false;
                }
                $scope.date = moment(value).utc().format('HH:mm');
                first_load = true;
            });

            $scope.$watch('date',function(value){
                if ( value==null ){
                    return false;
                }
                var result = value.split(':');
                if ( result.length!=2){
                    return false;
                }
                $scope.ngModel =  new Date( Date.UTC(2015,10,10,result[0],result[1]) );

            })
        }


    }

    angular.module('hotelBooking').directive('hotelDate', hotelDateDirective);


})(window.angular);
