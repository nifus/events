(function (angular) {
    'use strict';
    angular.module('events').service('eventFactory', eventFactory);

    eventFactory.$inject = [ 'eventService','categoryFactory','subCategoryFactory'];

    function eventFactory(eventService,categoryFactory,subCategoryFactory) {
        var cache = [];
        return {
            getEventById: getEventById,
            getAllEvents: getAllEvents,
            getCategoryEvents: getCategoryEvents,
            store: store
        };

        function store(data, callback) {
            var hotelObject = Parse.Object.extend("Event");
            var hotel = new hotelObject();
            var i;
            for (i in data) {
                hotel.set(i, data[i]);
            }


            hotel.save(null, {
                success: function (hotel) {
                    callback({success: true});
                },
                error: function (hotel, error) {
                    callback({success: false, error: error.message});
                }
            });
        }

        function getEventById(id,callback){
            categoryFactory.getAll( function(categories){
                subCategoryFactory.getAll( function(subCategories){
                    var hotel = Parse.Object.extend("Event");
                    var query = new Parse.Query(hotel);
                    query.include('Venue').get(id, {
                        success: function (results) {
                            callback(eventService(results,categories,subCategories))
                        },
                        error: function (error) {
                            alert("Error: " + error.code + " " + error.message);
                        }
                    });
                });
            });

        }

        function getAllEvents(callback){
            categoryFactory.getAll( function(categories){
                subCategoryFactory.getAll( function(subCategories){
                    var event = Parse.Object.extend("Event");
                    var query = new Parse.Query(event);
                    query.include('Venue').limit(1000);
                    query.find({
                        success: function (results) {
                            cache = results;
                            var events = [];
                            angular.forEach(results, function (row) {
                                var eventObject = eventService(row,categories,subCategories);
                                events.push(eventObject);
                            });

                            callback(events);
                        },
                        error: function (error) {
                            alert("Error: " + error.code + " " + error.message);
                        }
                    });
                });
            });

        }
        function getCategoryEvents(category,callback){

            categoryFactory.getAll( function(categories){
                subCategoryFactory.getAll( function(subCategories){
                    var event = Parse.Object.extend("Event");
                    var query = new Parse.Query(event);
                    query.include('Venue').containedIn('Category',[category]).limit(1000);
                    query.find({
                        success: function (results) {
                            cache = results;
                            var events = [];
                            angular.forEach(results, function (row) {
                                var eventObject = eventService(row,categories,subCategories);
                                events.push(eventObject);
                            });

                            callback(events);
                        },
                        error: function (error) {
                            alert("Error: " + error.code + " " + error.message);
                        }
                    });
                });
            });

        }

    }
})(angular);

