(function (angular) {
    'use strict';
    angular.module('events').service('eventService', eventService);

   // hotelService.$inject = [];
    function eventService() {
        return function (parseObject,categories,subCategories) {

            var object = parseObject.attributes;
            object.parseObject = parseObject;
            object.attributes = angular.copy(parseObject.attributes);

            object.attributes.createdAt = moment(object.parseObject.createdAt).format('YYYY-MM-DD');


            object.schedule = [];

            if ( object.attributes.start_Time!=undefined && object.attributes.start_Time.length>0 ){
                var i;
                for( i in object.attributes.start_Time ){
                    var begin = object.attributes.start_Time[i];
                    if ( object.attributes.end_Time==undefined ){
                        var end = 'Invalid date';
                    }else{
                        var end = object.attributes.end_Time[i];
                    }
                    if ( end=='Invalid date' ){
                        object.schedule.push(
                            {begin:begin,end:null}
                        )
                    }else{
                        object.schedule.push(
                            {begin:begin,end:end}
                        )
                    }
                }
            }else{
                console.log('event no end time')
            }
            object.expired = true;

            object.venueName  = object.parseObject.get('Venue').get('name');
            var now = moment().add('3','hours');
            angular.forEach( object.schedule, function(time){
                if ( object.expired==false ){
                    return false;
                }

                if ( time.end!==null && now.isBefore(time.end) ){
                    object.expired = false;
                }else if ( time.end==null && now.isBefore(time.begin) ){
                    object.expired = false;
                }
            });



           /* var cat_objects = [];
            angular.forEach( object.attributes.SubCategory,function(cat){
                if ( subCategories[cat]==undefined ){
                    return false;
                }
                cat_objects.push( subCategories[cat] )
            } );
            object.attributes.SubCategory = cat_objects;

            var cat_objects = [];
            angular.forEach( object.attributes.Category,function(cat){

                if ( categories[cat]==undefined ){
                    return false;
                }
                cat_objects.push( categories[cat] )
            } );
            object.attributes.Category = cat_objects;*/

            var cat_objects = {};
            angular.forEach( object.attributes.Category,function(cat){
                cat_objects[ cat ] = true
            } );
            object.attributes.Category = cat_objects;
            var cat_objects = {};
            angular.forEach( object.attributes.SubCategory,function(cat){
                cat_objects[ cat ] = true
            } );
            object.attributes.SubCategory = cat_objects;

            //console.log(  object.attributes.Category )
            object.get = function(key){
                return this.attributes[key];
            };
            object.getId = function(){
                return this.parseObject.id ;
            };

            object.getCategories = function(){
                var result = {};

                angular.forEach( object.attributes.Category,function(category){
                    result[category.get('name')] = [];
                } );
                angular.forEach( object.attributes.SubCategory,function(category){
                    result[category.get('Category').get('name')].push( category.get('name') )

                } );
                return result;
            }
            //object.attributes.cats = object.getCategories();

            object.update = function(data,callback){
                var i;

                for( i in data ){
                    this.parseObject.set(i,data[i]);
                }
                this.parseObject.save(data, {
                    success: function(answer) {
                        callback(answer);
                    },
                    error: function (answer, error) {
                        callback({success: false, error: error.message});
                    }
                })
            };

            object.delete = function(){
                this.parseObject.destroy({
                    success: function(myObject) {
                       console.log('delete')
                    },
                    error: function(myObject, error) {
                        // The delete failed.
                        // error is a Parse.Error with an error code and message.
                    }
                });
            }
            return object;
        }
    }
})(angular);

