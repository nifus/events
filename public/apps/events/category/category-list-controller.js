(function (angular) {
    'use strict';

    angular.module('eventsApp').
        controller('categoryListController', listController);

    listController.$inject = ['$scope', 'subCategoryFactory','userFactory','$location','categoryFactory'];
    function listController($scope, subCategoryFactory,userFactory,$location,categoryFactory) {

        $scope.env = {
            loading:true,
            categories: {}
        };
        userFactory.auth( function(user){
            $scope.env.user = user;
            if ( user===null ){
                    $location.path('/' );
                    $scope.$apply();
            }
        });
        categoryFactory.getAll(function (categories) {
            var i;
            for( i in categories ){
                var category = categories[i];
                    $scope.env.categories[ category.get('name')] = []
            }

            subCategoryFactory.getAll(function (subcategories) {

                var i;
                for( i in subcategories ){
                    var category = subcategories[i];
                    if ( $scope.env.categories[ category.get('Category').get('name')] == undefined ){
                        $scope.env.categories[ category.get('Category').get('name')] = []
                    }else{
                        $scope.env.categories[ category.get('Category').get('name')].push( category.get('name') ) ;
                    }
                }

                $scope.env.loading = false;
                $scope.$apply();
            });
        })


    }
})(angular);