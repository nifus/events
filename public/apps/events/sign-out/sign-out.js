(function (angular) {
    'use strict';

    angular.module('eventsApp').
        controller('signOutController', signOutController);

    signOutController.$inject = ['$scope','userFactory','$location','$rootScope'];
    function signOutController($scope, userFactory,$location,$rootScope) {

        userFactory.logout( function(){
            $rootScope.$broadcast('logout');
            $location.path('/');
        });




    }
})(angular);