(function (angular) {
    'use strict';

    angular.module('eventsApp').
        controller('formController', formController);

    formController.$inject = ['$scope', 'eventFactory', '$location', '$routeParams', '$rootScope', 'userFactory','categoryFactory','subCategoryFactory','venueFactory'];
    function formController($scope, eventFactory, $location, $routeParams, $rootScope, userFactory,categoryFactory,subCategoryFactory,venueFactory) {

        $scope.env = {
            categories:{},
            id: $routeParams.id,
            loading: true,
            saving: false,
            eventTimes:[],
            model: {
                time_Begin:[],
                time_End:[],
                Venue:{},
                Category:{},
                SubCategory:{},
            },
            error: null,
            user: null,
            venues:[]

        };
        venueFactory.getAll(function(result){
            $scope.env.venues = result;
            $scope.$apply();
        });
        $scope.changeVenue = function(value){
       //     $scope.env.model.Venue.name = value
            //console.log(value)
        }
        $scope.selectVenue = function(value){
           $scope.env.model.Venue = value.originalObject;
        }

        categoryFactory.getAll(function (categories) {
            var i;
            for( i in categories ){
                var category = categories[i];
                $scope.env.categories[category.id] = {id:category.id,name:category.get('name'),sub:[]}
            }

            subCategoryFactory.getAll(function (subcategories) {

                var i;
                for( i in subcategories ){
                    var category = subcategories[i];

                    $scope.env.categories[ category.get('Category').id]['sub'].push({
                        id:category.id,name:category.get('name')
                    });
                }
                $scope.$apply();

                $scope.env.loading = false;
            });
            $scope.$apply();
        });

        userFactory.auth(function (user) {
            $scope.env.user = user;
            if ( !user) {
                    $location.path('/');
                    $scope.$apply();

            }
            $scope.$apply();
        });

        if ($scope.env.id != undefined) {
            eventFactory.getEventById($scope.env.id, function (event) {
                $scope.env.loading = false;
                //$scope.initLinking(hotel.attributes);
                $scope.env.model = event.attributes;
                $scope.env.event = event;
                $scope.selectVenue = event.parseObject.get('Venue');
                $scope.$apply();

            });
        }



        $scope.save = function (data) {
            var categories = [];
            console.log(data)
            angular.forEach( data.Category,function(value,key){
                console.log(key)
                console.log(value)
                if ( angular.isNumber(key) ){
                    categories.push(value)
                }else{
                    categories.push(key)
                }
            } );
            data.Category = categories;

            var categories = [];
            angular.forEach( data.SubCategory,function(value,key){
                if ( angular.isNumber(key) ){
                    categories.push(value)
                }else{
                    categories.push(key)
                }
            } );
            data.SubCategory = categories;

            //return false;
            var i;
            for( i in data['time_Begin'] ){
                data['time_Begin'][i] = moment(data['time_Begin'][i]).utc().format('DD/MM/YYYY hh:mm a')
            }
            for( i in data['time_End'] ){
                data['time_End'][i] = moment(data['time_End'][i]).utc().format('DD/MM/YYYY hh:mm a')
            }

            $scope.env.saving = true;
            if ($scope.env.id != undefined) {
                $scope.env.event.update(data, callback);
            } else {
                eventFactory.store(data, callback)
            }

            function callback(result) {
                $scope.env.saving = false;
                if (result.success === false) {
                    $scope.env.error = result.error;
                    $scope.$apply();
                } else {
                    $rootScope.$apply(function () {
                        $location.path('/events');
                    });
                }
            }
        }

        $scope.addNewEventTime = function(){
            $scope.env.model.time_Begin.push(null )
            $scope.env.model.time_End.push(null)
        }
        $scope.removeNewEventTime = function(index){
            $scope.env.model.time_Begin.splice(index,1)
            $scope.env.model.time_End.splice(index,1)
        }


    }

})(angular);