(function (angular) {
    'use strict';

    angular.module('eventsApp').
        controller('listController', listController);

    listController.$inject = ['$scope', 'eventFactory', 'userFactory', '$location', '$filter', 'categoryFactory', 'subCategoryFactory', 'ipCookie'];
    function listController($scope, eventFactory, userFactory, $location, $filter, categoryFactory, subCategoryFactory, ipCookie) {

        $scope.env = {
            loading: true,
            events: [],
            tableConfig: {
                itemsPerPage: 50,
                fillLastPage: false
            },
            filters: {
                title: undefined,
                Category: 'ArQt0qOh81',
                SubCategory: undefined,
               // expired: false,
                venueName: undefined
            },
            restoreFilters: {},
            user: null,
            categories: [],
            subCategories: {}
        };

        var restoreFilter = ipCookie('filters');
        if ( restoreFilter==undefined ){
            restoreFilter = {
                title: undefined,
                Category: 'ArQt0qOh81',
                SubCategory: undefined,
                // expired: false,
                venueName: undefined
            }
        }
        angular.forEach(restoreFilter, function (value, key) {
            $scope.env.restoreFilters[key] = value
        })


        userFactory.auth(function (user) {
            $scope.env.user = user;
            if (user === null) {
                $location.path('/');
                $scope.$apply();
            }
        });
        eventFactory.getCategoryEvents($scope.env.filters.Category, function (result) {
            $scope.env.loading = false;
            $scope.env.events = result;

            $scope.env.eventsSource = result;
            $scope.$apply();
            $scope.restoreFilters();
        });

        $scope.$watchCollection('env.filters', function (value) {
            if ($scope.env.eventsSource == undefined) {
                return false;
            }
            $scope.env.events
                = $filter("filter")($scope.env.eventsSource, $scope.env.filters);
        }, true);

        $scope.delete = function (object) {
            var c = confirm("Delete event?");
            if (!c) {
                return false;
            }
            object.delete();
            $scope.env.events = $scope.env.events.filter(function(event){
                if ( event.getId()==object.getId() ){
                    return false;
                }
                return true;
            });
            $scope.env.eventsSource = $scope.env.eventsSource.filter(function(event){
                if ( event.getId()==object.getId() ){
                    return false;
                }
                return true;
            })
        };

        categoryFactory.getAll(function (categories) {
            var i;
            for (i in categories) {
                var category = categories[i];

                $scope.env.categories[$scope.env.categories.length] = {
                    id: category.id,
                    name: category.get('name'),
                    sub: ''
                }
            }
            $scope.$apply();
        });


        subCategoryFactory.getAll(function (subcategories) {
            var i;
            for (i in subcategories) {
                var category = subcategories[i];
                if ($scope.env.subCategories[category.get('Category').id] == undefined) {
                    $scope.env.subCategories[category.get('Category').id] = [
                        {id: category.id, name: category.get('name')}
                    ]
                } else {
                    $scope.env.subCategories[category.get('Category').id].push(
                        {id: category.id, name: category.get('name')}
                    );
                }
            }
            $scope.$apply();
        });

        $scope.changeFilterCategory = function () {
            $scope.env.filters.SubCategory = undefined;
        }
        $scope.clearFilters = function () {
            angular.forEach($scope.env.filters, function (value, key) {
                $scope.env.filters[key] = undefined;
            })
            $scope.env.filters = {
                title: undefined,
                Category: undefined,
                SubCategory: undefined,
                //expired: false
            };
        };
        $scope.restoreFilters = function () {

            $scope.env.filters = $scope.env.restoreFilters
            $scope.$apply();
        }


        $scope.$watchCollection('env.filters', function () {
            ipCookie('filters', $scope.env.filters);
        })

        $scope.$watch('env.filters.Category',function(value){
            eventFactory.getCategoryEvents($scope.env.filters.Category, function (result) {
                $scope.env.loading = false;
                $scope.env.events = result;

                $scope.env.eventsSource = result;
                $scope.$apply();
                $scope.restoreFilters();
            });
        })

    }
})(angular);