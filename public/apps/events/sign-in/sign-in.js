(function (angular) {
    'use strict';

    angular.module('eventsApp').
        controller('signInController', signInController);

    signInController.$inject = ['$scope','userFactory','$location','$rootScope'];
    function signInController($scope, userFactory,$location,$rootScope) {
        $scope.env = {
            error:null,
            sending:false,
            loading:true
        };
        $scope.actions = {
            login:login
        };
        userFactory.auth( function(user){
            $scope.env.loading = false;
            if ( user!=null ){
                $location.path('/events');
                $scope.$apply();
            }
        });


        function login(username,password){
            $scope.env.sending = true;
            userFactory.login(username,password,function(answer){
                if ( answer.success == false ){
                    $scope.env.error = answer.error;
                }else{
                    $rootScope.$broadcast('login');
                    $location.path('/events')
                }
                $scope.env.sending = false;
                $scope.$apply();
            })
        }


    }
})(angular);