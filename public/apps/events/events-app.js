(function (angular) {
    'use strict';
    angular.module('eventsApp', ['ngRoute', 'events', 'angular-table','naif.base64','ui.bootstrap.datetimepicker','ipCookie','angucomplete-alt']).
        config(function ($routeProvider) {
            $routeProvider.when('/', {
                templateUrl: 'apps/events/sign-in/sign-in.html',
                controller: 'signInController'
            }).when('/logout', {
                templateUrl: 'apps/events/sign-in/sign-in.html',
                controller: 'signOutController'
            }).when('/events', {
                templateUrl: 'apps/events/event/list.html',
                controller: 'listController'
            }).when('/events/create', {
                templateUrl: 'apps/events/event/form.html',
                controller: 'formController'
            }).when('/events/:id', {
                templateUrl: 'apps/events/event/form.html',
                controller: 'formController'
            }).when('/categories', {
                templateUrl: 'apps/events/category/list.html',
                controller: 'categoryListController'
            }).otherwise('/')

        });
})(angular);