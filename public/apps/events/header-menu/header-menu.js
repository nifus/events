(function (angular) {
    'use strict';

    function headerMenuDirective() {
        return {
            replace:true,
            restrict: 'E',
            controller:headerMenuController,
            templateUrl: '/apps/events/header-menu/header-menu.html',
            scope:{}
        };

        headerMenuController.$inject = ['$scope','userFactory','$rootScope'];
        function headerMenuController($scope,userFactory,$rootScope){
            auth();
            $rootScope.$on('login', function() {
                auth();
            });
            $rootScope.$on('logout', function() {
                auth();
            });
            function auth(){
                userFactory.auth(function(user){
                    $scope.user = user;
                })
            }
        }


    }

    angular.module('eventsApp').directive('headerMenu', headerMenuDirective);


})(window.angular);
